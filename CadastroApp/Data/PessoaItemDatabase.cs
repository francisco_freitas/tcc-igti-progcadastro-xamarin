﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;

namespace CadastroApp
{
	public class PessoaItemDatabase
	{
		readonly SQLiteAsyncConnection database;

public PessoaItemDatabase(string dbPath)
		{
			database = new SQLiteAsyncConnection(dbPath);
			database.CreateTableAsync<PessoaItem>().Wait();
		}

		public Task<List<PessoaItem>> GetItemsAsync()
		{
			return database.Table<PessoaItem>().ToListAsync();
		}

		public Task<List<PessoaItem>> GetItemsNotDoneAsync()
		{
			return database.QueryAsync<PessoaItem>("SELECT * FROM [PessoaItem]");
		}

		public Task<PessoaItem> GetItemAsync(int id)
		{
			return database.Table<PessoaItem>().Where(i => i.ID == id).FirstOrDefaultAsync();
		}

		public Task<int> SaveItemAsync(PessoaItem item)
		{
			if (item.ID != 0)
			{
				return database.UpdateAsync(item);
			}
			else {
				return database.InsertAsync(item);
			}
		}

		public Task<int> DeleteItemAsync(PessoaItem item)
		{
			return database.DeleteAsync(item);
		}
	}
}

