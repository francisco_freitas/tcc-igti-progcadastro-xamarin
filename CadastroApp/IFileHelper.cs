﻿namespace CadastroApp
{
	public interface IFileHelper
	{
		string GetLocalFilePath(string filename);
	}
}
