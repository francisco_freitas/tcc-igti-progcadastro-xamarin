﻿using System;
using SQLite;

namespace CadastroApp
{
	public class PessoaItem
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string nome { get; set; }
		public string cpf { get; set; }
		public DateTime dataNascimento { get; set; }
		public string telefone { get; set; }
	}
}

