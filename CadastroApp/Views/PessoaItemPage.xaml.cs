﻿using System;
using Xamarin.Forms;

namespace CadastroApp
{
	public partial class PessoaItemPage : ContentPage
	{
		public PessoaItemPage()
		{
			InitializeComponent();
		}

		async void OnSaveClicked(object sender, EventArgs e)
		{
			var pessoaItem = (PessoaItem)BindingContext;
			await App.Database.SaveItemAsync(pessoaItem);
			await Navigation.PopAsync();
		}

		async void OnDeleteClicked(object sender, EventArgs e)
		{
			var pessoaItem = (PessoaItem)BindingContext;
			await App.Database.DeleteItemAsync(pessoaItem);
			await Navigation.PopAsync();
		}

		async void OnCancelClicked(object sender, EventArgs e)
		{
			await Navigation.PopAsync();
		}

	}
}
