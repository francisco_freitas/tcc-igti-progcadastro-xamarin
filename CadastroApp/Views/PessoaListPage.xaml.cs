﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace CadastroApp
{
	public partial class PessoaListPage : ContentPage
	{
public PessoaListPage()
		{
			InitializeComponent();
		}

		protected override async void OnAppearing()
		{
			base.OnAppearing();

			// Reset the 'resume' id, since we just want to re-start here
			((App)App.Current).ResumeAtPessoaId = -1;
			listView.ItemsSource = await App.Database.GetItemsAsync();
		}

		async void OnItemAdded(object sender, EventArgs e)
		{
			var pessoaItemNovo = new PessoaItem();
			pessoaItemNovo.dataNascimento = DateTime.Now;


			await Navigation.PushAsync(new PessoaItemPage
			{
				BindingContext = pessoaItemNovo//new PessoaItem();
			});
		}

		async void OnListItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			((App)App.Current).ResumeAtPessoaId = (e.SelectedItem as PessoaItem).ID;
			Debug.WriteLine("setting ResumeAtTodoId = " + (e.SelectedItem as PessoaItem).ID);

			await Navigation.PushAsync(new PessoaItemPage
			{
				BindingContext = e.SelectedItem as PessoaItem
			});
		}
	}
}
