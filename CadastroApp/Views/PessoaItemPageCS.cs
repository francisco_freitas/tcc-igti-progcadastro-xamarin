﻿using Xamarin.Forms;

namespace CadastroApp
{
	public class PessoaItemPageCS : ContentPage
	{
		public PessoaItemPageCS()
		{
			Title = "Cadastro Pessoa";

			var nomeEntry = new Entry();
			nomeEntry.SetBinding(Entry.TextProperty, "nome");

			var cpfEntry = new Entry();
			cpfEntry.Keyboard = Keyboard.Numeric;
			cpfEntry.SetBinding(Entry.TextProperty, "cpf");

			var dataNascimento = new DatePicker();
			dataNascimento.SetBinding(DatePicker.DateProperty, "dataNascimento");

			var telefoneEntry = new Entry();
			telefoneEntry.Keyboard = Keyboard.Numeric;
			telefoneEntry.SetBinding(Entry.TextProperty, "telefone");

			var saveButton = new Button { Text = "Save" };
			saveButton.Clicked += async (sender, e) =>
			{
				var pessoaItem = (PessoaItem)BindingContext;
				await App.Database.SaveItemAsync(pessoaItem);
				await Navigation.PopAsync();
			};

			var deleteButton = new Button { Text = "Delete" };
			deleteButton.Clicked += async (sender, e) =>
			{
				var pessoaItem = (PessoaItem)BindingContext;
				await App.Database.DeleteItemAsync(pessoaItem);
				await Navigation.PopAsync();
			};

			var cancelButton = new Button { Text = "Cancel" };
			cancelButton.Clicked += async (sender, e) =>
			{
				await Navigation.PopAsync();
			};

			Content = new StackLayout
			{
				Margin = new Thickness(20),
				VerticalOptions = LayoutOptions.StartAndExpand,
				Children =
				{
					new Label { Text = "Nome" },
					nomeEntry,
					new Label { Text = "CPF" },
					cpfEntry,
					new Label{Text = "Data de Nascimento"},
					dataNascimento,

					new Label { Text = "Telefone" },
					telefoneEntry,
					saveButton,
					deleteButton,
					cancelButton,

				}
			};
		}
	}
}
