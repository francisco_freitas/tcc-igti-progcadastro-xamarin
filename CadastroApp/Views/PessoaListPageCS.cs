﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace CadastroApp
{
public class PessoaListPageCS : ContentPage
	{
		ListView listView;

		public PessoaListPageCS()
		{
			Title = "Cadastro";

			var toolbarItem = new ToolbarItem
			{
				Text = "+",
				Icon = Device.OnPlatform(null, "plus.png", "plus.png")
			};
			toolbarItem.Clicked += async (sender, e) =>
			{
				await Navigation.PushAsync(new PessoaItemPageCS
				{
					BindingContext = new PessoaItem().dataNascimento
				});
			};
			ToolbarItems.Add(toolbarItem);

			listView = new ListView
			{
				Margin = new Thickness(20),
				ItemTemplate = new DataTemplate(() =>
				{
					var label = new Label
					{
						VerticalTextAlignment = TextAlignment.Center,
						HorizontalOptions = LayoutOptions.StartAndExpand
					};
					label.SetBinding(Label.TextProperty, "nome");

					var tick = new Image
					{
						Source = ImageSource.FromFile("check.png"),
						HorizontalOptions = LayoutOptions.End
					};
					tick.SetBinding(VisualElement.IsVisibleProperty, "Done");

					var stackLayout = new StackLayout
					{
						Margin = new Thickness(20, 0, 0, 0),
						Orientation = StackOrientation.Horizontal,
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = { label, tick }
					};

					return new ViewCell { View = stackLayout };
				})
			};
			listView.ItemSelected += async (sender, e) =>
			{
				((App)App.Current).ResumeAtPessoaId = (e.SelectedItem as PessoaItem).ID;
				Debug.WriteLine("setting ResumeAtTodoId = " + (e.SelectedItem as PessoaItem).ID);

				await Navigation.PushAsync(new PessoaItemPageCS
				{
					BindingContext = e.SelectedItem as PessoaItem
				});
			};

			Content = listView;
		}

		protected override async void OnAppearing()
		{
			base.OnAppearing();

			// Reset the 'resume' id, since we just want to re-start here
			((App)App.Current).ResumeAtPessoaId = -1;
			listView.ItemsSource = await App.Database.GetItemsAsync();
		}
	}
}
