﻿using System;
using System.IO;
using Xamarin.Forms;
using CadastroApp.Droid;
using CadastroApp;

[assembly: Dependency(typeof(FileHelper))]
namespace CadastroApp.Droid
{
	public class FileHelper : IFileHelper
	{
		public string GetLocalFilePath(string filename)
		{
			string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			return Path.Combine(path, filename);
		}
	}
}
